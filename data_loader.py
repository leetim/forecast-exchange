import os
import re
import pandas as pd
import numpy as np
import h5py
import torch
from torch.utils.data import Dataset
from sklearn.model_selection import train_test_split
from torch import nn
import fastai
from fastai import basic_data
from fastai import basic_train

class NoFolderError(Exception):
    pass


class WrongLabelError(Exception):
    pass


class DataLoader:
    def __init__(self, path):
        self.path = path
        self.files = {i: h5py.File(f"{path}/{i}/data.h5py", 'r') for i in list(os.walk(path))[0][1] if re.match(r'\d{8}', i)}

    def get(self, folder):
        return self.get_df_from_folder(folder)

    def get_df_from_folder(self, folder_name):
        if folder_name in self.files:
            return self._get_df_from_file(
                self.files[folder_name]
            )
        else:
            raise NoFolderError(f'No folder {self.path}/{folder_name}')

    def _get_df_from_file(self, f):
        data = f['ObAndTrUpdates']
        ob = f['ObAndTrUpdates_cols']
        df = pd.DataFrame(data.value, columns=[i.decode('utf-8') for i in ob.value])
        return df

    def __iter__(self):
        for f in self.files:
            yield self._get_df_from_file(f)


def param_padding(x, target_shape):
    if x.shape == target_shape:
        return x
    n, m = x.shape
    new_n, _ = target_shape
    if n > new_n:
        return x[:new_n - n, :]
    else:
        mean_value = x.mean(axis=0)
        pad_part = np.ones((new_n - n, 1)) @ mean_value.reshape(1, m)
        result = np.zeros(target_shape)
        result[:n, :] = x[::-1, :]
        result[n:, :] = pad_part
        return result


class ArrayDataset(Dataset):
    "Sample numpy array dataset"
    def __init__(self, x, y, c=2):
        self.x, self.y = x, y
        self.c = c # binary label
    
    def __len__(self):
        return len(self.x)
    
    def __getitem__(self, i):
        return self.x[i], self.y[i]


def prepare_data(data, target_exchange, max_len=None):
    # Разбиение dataseta на куски
    df_exchange = data[np.logical_not(pd.isna(data.ask_p_0))].filter(items=[
        'ask_p_0', 'ask_p_1', 'ask_p_2', 'ask_p_3', 'ask_p_4', 'ask_v_0',
        'ask_v_1', 'ask_v_2', 'ask_v_3', 'ask_v_4', 'bid_p_0', 'bid_p_1',
        'bid_p_2', 'bid_p_3', 'bid_p_4', 'bid_v_0', 'bid_v_1', 'bid_v_2',
        'bid_v_3', 'bid_v_4', 'received_time', 'exchange'
    ])
    df_price = data[pd.isna(data.ask_p_0)].filter(items=[
        'received_time', 'tr_price',
        'tr_side', 'exchange'
    ])

    # Преобразование времени к полуминутам
    df_price.received_time = df_price.received_time.apply(int) // 30000
    df_exchange.received_time = df_exchange.received_time.apply(int) // 30000

    # Группировка и фильтр по валюте
    it_exch = df_exchange[df_exchange.exchange.astype('int64') == target_exchange].groupby('received_time')
    it_price = df_price[df_price.exchange.astype('int64') == target_exchange].groupby('received_time')

    # Вычисление имеющихся 
    x_inds = [i for i, _ in it_exch]
    y_dict = {i: v.tr_price.mean() for i, v in it_price}

    # Получение тензора входных параметров
    x = [v.drop(columns=['received_time', 'exchange']).values for i, v in it_exch if i + 1 in y_dict]
    if max_len is None:
        max_len = max(i.shape for i in x)
    x = torch.tensor(
        [param_padding(i, max_len) for i in x]
    )

    # Получение тензора результатов
    y = torch.tensor([y_dict[i + 1] for i in x_inds if i + 1 in y_dict])
    return x, y


def learner_generator(model, x, y, c, loss_func=nn.MSELoss(), random_state=42, test_size=.15, bs=bs):
    x, y = x.numpy(), y.numpy()
    data_split = train_test_split(
        x,
        y,
        random_state=random_state,
        test_size=test_size
    )
    x_train, x_valid, y_train, y_valid = [torch.from_numpy(i).float() for i in data_split]
    ds_train = ArrayDataset(x_train, y_train, c)
    ds_valid = ArrayDataset(x_valid, y_valid, c)
    data_bunch = basic_data.DataBunch.create(ds_train, ds_valid, device=torch.device('cuda'), bs=64)
    learner = basic_train.Learner(data_bunch, model, loss_func=loss_func)
    return learner

all_folders = [
    '20191001', '20191003', '20191005', '20191007', '20191009', '20191011', '20191013', '20191015', '20191017',
    '20191019', '20191021', '20191023', '20191025', '20191027', '20191029', '20191031', '20191002', '20191004',
    '20191006', '20191008', '20191010', '20191012', '20191014', '20191016', '20191018', '20191020', '20191022',
    '20191024', '20191026', '20191028', '20191030',
]
