# forecast-exchange

Ссылка на данные [zip](https://drive.google.com/uc?export=download&confirm=su2B&id=1PaS_G9B_QelF2-AvXOA0Rj1F6MuNpfeV) и [не zip](https://drive.google.com/open?id=132WDhqw54JMbOOWLmLeYEt8NgZrchOWC)

- Антон [код](https://colab.research.google.com/drive/12fwnyg6ZUBH3Dgt22SFQEN4-GQl0JEMc)
- Илья Л [код](https://colab.research.google.com/drive/1hgpWHYnQvnVmqw3owZhuku1ZDF_uvXti)
## DataLoader пример

```python
import data_loader as dl
loader = dl.DataLoader('./data')
df = loader.get('20191001')
```
