fastai==1.0.59
h5py==2.10.0
matplotlib==3.1.2
numpy==1.17.4
pandas==0.25.3
requests==2.22.0
scipy==1.3.3
torch==1.3.1
torchvision==0.4.2
